import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import store from './store'

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import CreateItem from './components/CreateItem.vue';
import DisplayItem from './components/DisplayItem.vue';
import EditItem from './components/EditItem.vue';
import Login from './components/Auth/Login.vue'
import Home from './components/Admin/Home.vue';

/**
 * Se definen las rutas
 */
const routes = [
  {
    name: 'DisplayItem',
    path: '/',
    component: DisplayItem,
    meta: {
      administrador: true,
      almacenero: true,
      vendedor: true
    }
  },
  {
    name: 'CreateItem',
    path: '/create/item',
    component: CreateItem
  },
  {
    name: 'EditItem',
    path: '/edit/:id',
    component: EditItem
  },
  {
    name: 'login',
    path: 'login',
    component: Login,
    meta: {
      libre: true
    }
  }
  ,
  {
    name: 'home',
    path: 'home',
    component: Home,
    meta: {
      administrador: true,
      almacenero: true,
      vendedor: true
    }
  }
];

const router = new VueRouter({ mode: 'history', routes: routes });
/**
 * Guardian
 */
router.beforeEach((to, from, next) => {
  const token = localStorage.getItem("token");
  console.log('Vamos bine');
  console.log('TOKEN:', token);
  if (to.matched.some(record => record.meta.libre)) {
    next();
    //} else if ( store.state.usuario && store.state.usuario.rol == 'Administrador'){
  } else if (token) {

    if (to.matched.some(record => record.meta.administrador)) {
      next();
    }
    //  } else if ( store.state.usuario && store.state.usuario.rol == 'Vendedor'){
  } else if (token) {

    if (to.matched.some(record => record.meta.vendedor)) {
      next();
    }
    //} else if ( store.state.usuario && store.state.usuario.rol == 'Almacenero'){
  } else if (token) {

    if (to.matched.some(record => record.meta.almacenero)) {
      next();
    }
  } else {
    console.log('Yes');
    next({ name: 'login' });
  }
})
/**
 * 
 */
new Vue(Vue.util.extend({ router, store }, App)).$mount('#app');
