
//const express=require('express');//ES5
import express  from 'express'; // ES6
//const morgan=require('morgan');//ES5
import morgan from 'morgan'; //ES6
//const cors=require('cors');//ES5
import  cors  from "cors";//ES6
import path from "path";
import mongoose from 'mongoose';
import router from './routes';

//Conexión a la base de datos MongoDB
mongoose.Promise=global.Promise;
const dbUrl = 'mongodb://localhost:27017/mevn';
mongoose.connect(dbUrl)
.then(mongoose => console.log('Conectado a la BD en el puerto 27017'))
.catch(err => console.log(err));

const app=express();
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(express.static(path.join(__dirname,'public')));

// Rutas de los EndPoints
app.use('/api',router);

app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'),()=>{
    console.log('El servidor se esta ejecutando ' + app.get('port'));
});

